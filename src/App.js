import React, { Component } from "react";
import "./App.css";
import utils from "./utils";

class App extends Component {
  renderParagraph = (paragraph, i) => {
    return (
      <p key={i}>
        {paragraph.map(item => {
          if (typeof item !== "string") {
            return <span key={i++}>{item}</span>;
          }
          return item;
        })}
      </p>
    );
  };

  render() {
    const text = "Meu texto está erado\nSegundo palagrafo";
    const mistakes = [
      {
        start: 15,
        end: 20,
        paragraph: 0
      },
      {
        start: 8,
        end: 17,
        paragraph: 1
      }
    ];

    // ========================================================================
    // Sua tarefa é implementar a função que converte as variáveis `text` e
    // `mistakes` na variável `textWithMistakes`, para textos e erros
    // arbitrários. Você deve fazer isso na função
    // `makeTextWithMistakes(text, mistakes)` usando outras funções de ajuda se
    // achar necessário
    // ========================================================================
    const textWithMistakes = utils.makeTextWithMistakes(text, mistakes);
    // const textWithMistakes = [
    //   ["Meu texto está ", <em>erado</em>],
    //   ["Segundo ", <em>palagrafo</em>],
    //   ["Segundo ", 2, 3]
    // ];

    return (
      <div className="App">
        <h3>Correção Textual</h3>
        <div className="document">
          <div className="content">
            {textWithMistakes.map(this.renderParagraph)}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
