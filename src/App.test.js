import React from "react";
import ReactDOM from "react-dom";
import utils from "./utils";

it("makeTextWithMistakes should be a function", () => {
  expect(typeof utils.makeTextWithMistakes).toBe("function");
});

it("makeTextWithMistakes('',[]) should be return []", () => {
  expect(utils.makeTextWithMistakes("", [])).toEqual([[""]]);
});

it("makeTextWithMistakes('Meu texto está erado',[]) should be return ['Meu texto está erado']", () => {
  expect(utils.makeTextWithMistakes("Meu texto está erado", [])).toEqual([
    ["Meu texto está erado"]
  ]);
});

it(`makeTextWithMistakes('Meu texto está erado',[{ start: 15, end: 20, paragraph: 0 }]) 
    should be return [['Meu texto está, <em>erado</em>']]`, () => {
  expect(
    utils.makeTextWithMistakes("Meu texto está erado", [
      {
        start: 15,
        end: 20,
        paragraph: 0
      }
    ])
  ).toEqual([["Meu texto está ", <em>erado</em>]]);
});

it(`makeTextWithMistakes('Meu texto está erado\nSegundo palagrafo',[])
    should be return [["Meu texto está erado"], ["Segundo palagrafo"]]`, () => {
  expect(
    utils.makeTextWithMistakes("Meu texto está erado\nSegundo palagrafo", [])
  ).toEqual([["Meu texto está erado"], ["Segundo palagrafo"]]);
});

it(`makeTextWithMistakes('Meu texto está erado\nSegundo palagrafo',[{
    start: 15,
    end: 20,
    paragraph: 0
  },
  {
    start: 8,
    end: 17,
    paragraph: 1
  }])
  should be return [["Meu texto está, <em>erado</em>"], ["Segundo, <em>palagrafo</em>"]]`, () => {
  expect(
    utils.makeTextWithMistakes("Meu texto está erado\nSegundo palagrafo", [
      {
        start: 15,
        end: 20,
        paragraph: 0
      },
      {
        start: 8,
        end: 17,
        paragraph: 1
      }
    ])
  ).toEqual([
    ["Meu texto está ", <em>erado</em>],
    ["Segundo ", <em>palagrafo</em>]
  ]);
});

it(`makeTextWithMistakes('Mru texto está errado\nSegundo palagrafo está errado',[{
    start: 0,
    end: 3,
    paragraph: 0
  },
  {
    start: 8,
    end: 17,
    paragraph: 1
  }])
  should be return [[<em>Mru</em>, " texto está errado"], ["Segundo ", <em>palagrafo</em>, " está errado"]]`, () => {
  expect(
    utils.makeTextWithMistakes(
      "Mru texto está errado\nSegundo palagrafo está errado",
      [
        {
          start: 0,
          end: 3,
          paragraph: 0
        },
        {
          start: 8,
          end: 17,
          paragraph: 1
        }
      ]
    )
  ).toEqual([
    [<em>Mru</em>, " texto está errado"],
    ["Segundo ", <em>palagrafo</em>, " está errado"]
  ]);
});

it(`makeTextWithMistakes('Mru texto está erado',[{
    start: 0,
    end: 3,
    paragraph: 0
  },
  {
    start: 15,
    end: 20,
    paragraph: 0
  }])
  should be return [[<em>Mru</em>, " texto está ", <em>erado</em>]]`, () => {
  expect(
    utils.makeTextWithMistakes("Mru texto está erado", [
      {
        start: 0,
        end: 3,
        paragraph: 0
      },
      {
        start: 15,
        end: 20,
        paragraph: 0
      }
    ])
  ).toEqual([[<em>Mru</em>, " texto está ", <em>erado</em>]]);
});

it(`makeTextWithMistakes('Mru texto está erado\nSegundo palagrafo está erado',[{
    start: 0,
    end: 3,
    paragraph: 0
  },
  {
    start: 15,
    end: 20,
    paragraph: 0
  },
  {
    start: 8,
    end: 17,
    paragraph: 1
  },
  {
    start: 23,
    end: 28,
    paragraph: 1
  }])
  should be return [[<em>Mru</em>, " texto está ", <em>erado</em>]]`, () => {
  expect(
    utils.makeTextWithMistakes(
      "Mru texto está erado\nSegundo palagrafo está erado",
      [
        {
          start: 0,
          end: 3,
          paragraph: 0
        },
        {
          start: 15,
          end: 20,
          paragraph: 0
        },
        {
          start: 8,
          end: 17,
          paragraph: 1
        },
        {
          start: 23,
          end: 28,
          paragraph: 1
        }
      ]
    )
  ).toEqual([
    [<em>Mru</em>, " texto está ", <em>erado</em>],
    ["Segundo ", <em>palagrafo</em>, " está ", <em>erado</em>]
  ]);
});
