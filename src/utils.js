import React from "react";

const makeTextWithMistakes = (text, mistakes) => {
  const paragraphs = text.split("\n");
  if (mistakes.length === 0) {
    return paragraphs.map(item => [item]);
  }
  return paragraphs.map((paragraph, indexParagraph) => {
    const mistakesToCurrentParagraph = mistakes.filter(
      mistake => mistake.paragraph === indexParagraph
    );
    if (!mistakesToCurrentParagraph) {
      return [paragraph];
    }
    const ExplodParagraph = paragraph.split("");
    return ExplodParagraph.reduce(
      (textWithMistakes, character, index) => {
        return verifyMistakes(
          mistakesToCurrentParagraph,
          textWithMistakes,
          character,
          index
        );
      },
      { count: 0, countMistakes: 0, arr: [""], wordWrong: "" }
    ).arr; // vai retornar o atributo arr do objeto passado para o reduce, desta forma o array com os textos e seus mistakes
  });
};

const verifyMistakes = (mistakes, text, character, index) => {
  if (
    index >= mistakes[text.countMistakes].start &&
    index <= mistakes[text.countMistakes].end - 1
  ) {
    text.wordWrong += character;
    if (index === mistakes[text.countMistakes].end - 1) {
      text.count = mistakes[text.countMistakes].start === 0 ? 0 : ++text.count;
      text.arr[text.count] = <em>{text.wordWrong}</em>;
      text.wordWrong = "";
      text.count++;
      text.countMistakes =
        mistakes.length > text.count ? text.count : mistakes.length - 1;
    }
  } else {
    if (!text.arr[text.count]) {
      text.arr[text.count] = "";
    }
    text.arr[text.count] += character;
  }
  return text;
};

export default {
  makeTextWithMistakes
};
